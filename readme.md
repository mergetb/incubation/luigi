# Merge Underlays L3

The diagram below shows the network we're building with VRFs. VRFs are described
as VLANs for layer 3 - so in esennce each of your physical routers can appear as
several independent routers.

![](diagram.png)

In the diagram above, a topology consisting of

- 1 core router
- 2 edge routers
- 6 nodes per edge routers

is virtual split in half into blue and green networks composed of

- 2 core routers (1 per virtual network)
- 4 edge switches (2 per virtual network)
- 3 nodes per virtual edge router

