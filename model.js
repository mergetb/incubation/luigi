node = Range(12).map(i => deb('x'+i))
leaf = Range(2).map(i => cx('l'+i))
spine = Range(1).map(i => cx('s'+i))

topo = {
    name: 'vrf',
    nodes: [
        ...node,
        ...leaf,
        ...spine,
    ],
    links: [
        ...node.map((x,i) => v2v(x.name, 1, leaf[Math.floor(i/6)].name, i%6)),
        v2v('l0', 6, 's0', 1),
        v2v('l1', 6, 's0', 2),
    ]
}
function cx(name) {
  return {
    name: name,
    image: 'cumulusvx-4.0',
    cpu: { cores: 2 },
    memory: { capacity: GB(2) }
  };
}
function deb(name) {
  return {
    name: name,
    image: 'debian-bullseye',
    memory: { capacity: GB(4) },
    cpu: { cores: 2 },
  };
}
function v2v(a, ai, b, bi, props={}) {
  lnk = Link(a, ai, b, bi, props)
  lnk.v2v = true
  return lnk
}
